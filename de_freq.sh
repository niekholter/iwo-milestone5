#!/bin/bash
#
# Descr: find every occurance for the non-substring word 'de'
#        and prints the amount of matches on the users screens.
#
# Usage: ./de_freq.sh FILE

# Checks if first commandline argument is file
TEXT=$1
if [ -z "$TEXT" ]
then
    echo "specify a file!"
    exit
fi

# Finds all occurances of de and gives amount of occurances
grep -wo 'de' $TEXT | wc -l
